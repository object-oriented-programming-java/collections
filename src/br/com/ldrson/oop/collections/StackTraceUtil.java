package br.com.ldrson.oop.collections;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by leanderson on 10/06/16.
 */
public class StackTraceUtil {

    public static String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return "\n"+result.toString();
    }

}
