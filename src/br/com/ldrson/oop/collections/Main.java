package br.com.ldrson.oop.collections;

/**
 * Created by leanderson on 10/06/16.
 */
public class Main {

    public static void main(String args[]){

        System.out.println("|-----------------------------------------------|");
        System.out.println("|               Java Collections                |");
        System.out.println("|-----------------------------------------------|");

        ExemploArranjo.demonstrarComoSeUtilizaArranjo();

        ExemploArrayList.demonstrarComoSeUtilizaArrayList();


    }

}
