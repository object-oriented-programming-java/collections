package br.com.ldrson.oop.collections;

import java.util.Date;

/**
 * Created by leanderson on 10/06/16.
 */
public class ExemploArranjo {

    public static void demonstrarComoSeUtilizaArranjo(){


        System.out.println("|===============================================|");
        System.out.println("|               Arranjos                        |");
        System.out.println("|===============================================|");
        System.out.println("| Os arranjos possuem tamanho fixo e precisa ser informado na instanciação do objeto");
        System.out.println("|-----------------------------------------------|");
        System.out.println("| Declaração : int arranjo[] = new int[4];      |");
        System.out.println("|    ou      : int arranjo[] = {1, 2, 3 , 4};   |");

        int arranjo[] = new int[4];

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Os arranjos podem ser criados com qualquer tipo primitivo ou classes.");

        String arranjoDeStrings[] = new String[4];
        double arranjoDeDoubles[] = new double[4];
        Date arranjoDeDates[] = new Date[4];


        System.out.println("|-----------------------------------------------|");
        System.out.println("| Verificar o tamanho do arranho : int tamanhoDoArranjo = arranjo.length;");

        System.out.println("| tamanho do arranjo = "+arranjo.length);
        System.out.println("|-----------------------------------------------|");

        System.out.println("| Um laço para definir os valores de cada posiçao do arranjo");
        System.out.println("| Para acessar o valor de uma posiçao : arranjo[posicao]");
        System.out.println("| A primeira posiçao do arranjo e 0.");
        System.out.println("| A ultima posiçao do arranjo e (tamanho - 1) .");
        System.out.println("|-----");

        for(int i = 0; i < arranjo.length; i++){
            arranjo[i] = i * i;
            System.out.println("| Posiçao "+i+", valor = "+arranjo[i]);
        }

        System.out.println("|-----------------------------------------------|");
        System.out.println("| O arranjo não tem a capacidade de crescer conforme a necessidade.");
        System.out.println("| Caso seja necessário mais posições é preciso criar um novo arranjo.");
        System.out.println("|-----------------------------------------------|");
        System.out.println("| O acesso em uma posição inválida do arranjo dispara uma exceção.");
        System.out.println("| A exceção se chama ArrayIndexOutOfBoundsException e na mensagem de erro exibe a posição em que foi tentado acessar.");

        try{
            int a = arranjo[99]; // Tentativa de acessar a posição 99 em um arranjo com 4 posições.
        }catch (Exception e){
            System.out.println(StackTraceUtil.getStackTrace(e.fillInStackTrace()));
        }


        System.out.println("|-----------------------------------------------|");




    }

}
