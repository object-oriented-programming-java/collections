package br.com.ldrson.oop.collections;

import java.util.ArrayList;

/**
 * Created by leanderson on 10/06/16.
 */
public class ExemploArrayList {

    public static void demonstrarComoSeUtilizaArrayList(){

        System.out.println("|===============================================|");
        System.out.println("|               ArrayList                       |");
        System.out.println("|===============================================|");
        System.out.println("| ArrayList é uma estrutura de dados sostificada que simula um arranjo de objetos com tamanho \"infinito\". ");
        System.out.println("|-----------------------------------------------|");
        System.out.println("| Declaração : ArrayList<String> lista = new ArrayList<>() ");
        System.out.println("| OBS : nas versões anteriores ao Java 8 é preciso declarar assim: \n" +
                "   ArrayList<String> lista = new ArrayList<String>() ");
        System.out.println("| OBS : Generics está disponível a partir do Java 1.5");

        ArrayList<String> lista = new ArrayList<>();

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Os ArrayList só podem ser criados para classes.");
        System.out.println("| Para utilizar tipos primitivos deve ser utilizados as classes Wrapper");
        System.out.println("| Exemplo : ArrayList<Integer> listaDeInteiros = new ArrayList<>();");

        ArrayList<Integer> listaDeInteiros = new ArrayList<>();
        ArrayList<Double> listaDeDecimais = new ArrayList<>();
        ArrayList<Boolean> listaDeBooleanos = new ArrayList<>();

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para adicionar um objeto na lista é utilizdo o método add().");
        System.out.println("| Exemplo : lista.add(\"Joinville\");");

        lista.add("Joinville");
        lista.add("São Francisco do Sul");
        lista.add("Araquari");

        System.out.println("|-----------------------------------------------|");
        System.out.println("| É possível adionar um objeto em uma posição específica com método add().");
        System.out.println("| Exemplo : Adicionar a string \"Manaus\" na posição 1 => lista.add(1, \"Manaus\");");

        String manaus = "Manaus";

        lista.add(1,manaus);

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para verificar o tamanho da lista é utilizado o metódo size()");
        System.out.println("| Exemplo : lista.size(); ");
        System.out.println("| Tamanho da lista de strings = "+lista.size());


        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para recuperar um item da lista é utilizado o método get()");
        System.out.println("| Exemplo : lista.get(posicao); ");
        System.out.println("| String da posição 1 = "+lista.get(1));


        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para listar os itens da lista pode se utilizar um laço");
        System.out.println("|--");

        for(String item : lista){
            System.out.println("| "+item);
        }

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para modificar um item da lista é utilizado o método set");
        System.out.println("| Exemplo : lista.set(posicao, objeto); ");

        lista.set(0,"Londres");


        System.out.println("| Exemplo : String da posição 0 é : "+lista.get(0));

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para verificar se a lista possui um determinado objeto é utilizado o método contains()");
        System.out.println("| Exemplo : lista.contains(objeto); ");
        System.out.println("| A lista de strings contêm a String \"Londres\" ? Resposta : "+lista.contains("Londres"));


        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para verificar a posição de um determinado objeto é utilizado o método indexOf()");
        System.out.println("| Exemplo : lista.indexOf(objeto); ");
        System.out.println("| Qual é a posição da String \"Londres\" ? Resposta : "+lista.indexOf("Londres"));
        System.out.println("|--");
        System.out.println("| Se o objeto não existir na lista, retorna o valor -1");
        System.out.println("| Qual é a posição da String \"Paris\" ? Resposta : "+lista.indexOf("Paris"));

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para remover um item da lista é utilizado o método remove()");
        System.out.println("| Exemplo : lista.remove(posicao); ");
        System.out.println("|--");

        lista.remove(1);


        for(String item : lista){
            System.out.println("| "+item);
        }

        System.out.println("|-----------------------------------------------|");
        System.out.println("| É possivel passar para o método remove() o objeto que deseja remover");
        System.out.println("| Exemplo : lista.remove(manaus); ");
        System.out.println("|-----");
        System.out.println("| Lista completa");
        System.out.println("|--");

        lista.add(1,manaus); // adiciona o item novamente na lista

        for(String item : lista){
            System.out.println("| "+item);
        }

        System.out.println("|-----");
        System.out.println("| Lista atualizada após remover o item ");
        System.out.println("|--");

        lista.remove(manaus);

        for(String item : lista){
            System.out.println("| "+item);
        }


        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para remover todos os itens da lista é utilizado o método clear()");
        System.out.println("| Exemplo : lista.clear(); ");

        lista.clear();

        System.out.println("| Tamanho da lista de strings = "+lista.size());

        System.out.println("|-----------------------------------------------|");
        System.out.println("| Para verificar se a lista está vazia é utilizado o método isEmpty()");
        System.out.println("| Exemplo : lista.isEmpty(); ");
        System.out.println("| A lista de strings está vazia ? Resposta : "+lista.isEmpty());

        System.out.println("|-----------------------------------------------|");
        System.out.println("| ERROS COMUNS");
        System.out.println("|-----------------------------------------------|");
        System.out.println("| Utilizar um arraylist que não foi inicializado. Vai lançar a excpetion NullPointerException.");
        try{
            ArrayList<String> umaNovaLista = null;

            umaNovaLista.size();

        }catch (NullPointerException e){
            System.out.println(StackTraceUtil.getStackTrace(e.fillInStackTrace()));
        }
        System.out.println("| Solução : Inicializar o arraylista na declaração do objeto. ");

        ArrayList<String> umaNovaLista = new ArrayList<>();

        System.out.println("|-----------------------------------------------|");

        System.out.println("| Utilizar um objeto da lista que está nulo. Vai lançar a excpetion NullPointerException.");

        String umaStringNula = null;

        umaNovaLista.add(umaStringNula);

        try{

            // transformar a string da posição 0 em letras maiusculas
            umaNovaLista.get(0).toUpperCase();

        }catch (NullPointerException e){
            System.out.println(StackTraceUtil.getStackTrace(e.fillInStackTrace()));
        }

        System.out.println("| Solução : Verificar se o objeto está nulo antes de usá-lo. ");

        String umaString = umaNovaLista.get(0);

        // Verifica se a string está nula
        if(umaString != null){
            // transformar a string da posição 0 em letras maiusculas
            umaString.toUpperCase();
        }else{
            System.out.println("| A string da posição 0 da lista umaNovaLista está nulo.");
        }

        System.out.println("|-----------------------------------------------|");

        System.out.println("| O acesso em uma posição inválida da lista dispara uma exceção.");
        System.out.println("| A exceção se chama IndexOutOfBoundsException e na mensagem de erro exibe a posição em que foi tentado acessar e o tamanho da lista.");

        try{
            umaNovaLista.get(99); // Tentativa de acessar a posição 99 em uma lista com 1 objeto.
        }catch (Exception e){
            System.out.println(StackTraceUtil.getStackTrace(e.fillInStackTrace()));
        }


    }

}
