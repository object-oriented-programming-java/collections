# Trabalhando com coleções em Java

## Ferramenta utilizada

Projeto Java criado com Intellij Idea Community <img src="http://s18.postimg.org/6gcq0qtih/intellij_idea_logo_400x400.png" width="48">

Disponível gratuitamente em https://www.jetbrains.com/idea/

--
## Como executar o projeto

Execute o projeto a partir do método main da Classe [Main](https://gitlab.com/object-oriented-programming-java/collections/blob/master/src/br/com/ldrson/oop/collections/Main.java)

## Exemplos Disponíveis

### Arranjos

Classe [ExemploArranjo](https://gitlab.com/object-oriented-programming-java/collections/blob/master/src/br/com/ldrson/oop/collections/ExemploArranjo.java)

### ArrayList

Classe [ExemploArrayList](https://gitlab.com/object-oriented-programming-java/collections/blob/master/src/br/com/ldrson/oop/collections/ExemploArrayList.java)

Exemplos com os métodos **add, get, size, set, remove, contains, indexOf, clear, isEmpty, contains.**
